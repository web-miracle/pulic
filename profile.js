const _ = require('lodash')
const pcName = process.env.COMPUTERNAME

const DEFAULT = {
	name: 'Не определен',
	host: 'http://libs/'
}
const USERS = [
	{
		name: 'Дмитрий Новиков',
		pc: 'DESKTOP-SM221FF',
		host: 'http://videocollaba/'
	}
]

let USER = _.find(USERS, item => item.pc === pcName)
if (!USER) USER = DEFAULT

const profiles = [
// ============================================
//
//	 Профиль всех разработчиков,
//  проксирует апи во избежания проблем с CORS
//
// =============================================
	{
		typeName: 'DEV - Режим разработки',
		arg: '--dev',
		developer: USER.name,
		axios: {
			proxy: true
		},
		proxy: {
			'/laravel/storage/app/public/': USER.host,
		},
		router: {
			base: '/'
		},
		generate: {
			dir: ''
		}
	},

	// ===============================
	//
	//	Генерирует статику в php, заточеную
	// для работы в Turbo /api-php-path/
	// и /base-path/ - константой из php
	//
	// ===============================
	{
		typeName: 'Генерация php статики',
		arg: '--php',
		developer: USER.name,
		axios: {
			baseURL: USER.host,
			browserBaseURL: '/api-php-path/',
			proxy: false
		},
		proxy: {},
		router: {
			base: '/base-path/'
		},
		generate: {
			dir: '../dist'
		}
	},
	{
		typeName: 'Генерация php статики',
		arg: '--html',
		developer: USER.name,
		axios: {
			baseURL: USER.host,
			browserBaseURL: '/',
			proxy: false
		},
		proxy: {},
		router: {
			base: '/'
		},
		generate: {
			dir: '../dist'
		}
	},
	// ===============================
	//
	//
	//
	// ===============================
	{
		typeName: 'SSR - для боя',
		arg: '--ssr',
		developer: USER.name,
		axios: {
			baseURL: '/', // URL для сервера,
			browserBaseURL: '/', // URL для браузера,
			proxy: false
		},
		proxy: {},
		router: { 
			base: '/'
		},
		generate: {
		}
	}
]

let profile = profiles[0]

profiles.forEach(item => {
	let test = _.some(process.argv, arg => arg === item.arg)
	if (test) {
		profile = item
	}
})

console.log(`

	Тип запуска: ${profile.typeName}
	Разработчик: ${profile.developer}

`)

module.exports = profile
