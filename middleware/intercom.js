export default async () => {
	if (process.browser && window && window.Intercom)
		window.Intercom("update");
}