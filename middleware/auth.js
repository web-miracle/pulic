export default function ({ store, route, redirect }) {
	if (!store.getters['user/isAuth']) {
		store.commit('user/SET_ERROR', {
			auth_error: `Для просмотра страницы http://staya.trading${route.path} необходимо зайти в свою учетную запись`
		})

		return redirect('/')
	}
}