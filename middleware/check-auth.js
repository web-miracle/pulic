import axios from 'axios'
const base = require('~/plugins/axios.conf.json').baseURL

export default async function ({ store, req }) {
	if (process.server) {
		if (!req || !req.headers.cookie) {
			store.commit('user/TOKEN_FAIL')
			return
		}

		const jwtCookie = req.headers.cookie.split(';').find(c => c.trim().startsWith('secret='))
		if (!jwtCookie) {
			store.commit('user/TOKEN_FAIL')
			return
		}

		const jwt = jwtCookie.split('=')[1]

		if (jwt !== 'null') {
			store.commit('user/SET_COOKIE_TOKEN', jwt)
			await store.dispatch('user/checkUser', jwt, {root: true})
		} else {
			store.commit('user/TOKEN_FAIL')
		}
	}

	return
}