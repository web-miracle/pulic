export default function ({ req, store, redirect }) {
	if (process.server) {
		if (!req || !req.headers.cookie) {
			return redirect('/promo/intership')
		}

		let promoID = req.headers.cookie.split(';').find(c => c.trim().startsWith('promoID='))
		if (!promoID) {
			return redirect('/promo/intership')
		}

		promoID = promoID.split('=')[1]

		if (promoID !== 'null') {
			store.commit('user/SET_PROMOID', promoID)
			return
		}

		return redirect('/promo/intership')
	} else {
		if (!store.user.state.promoID) {
			return redirect('/promo/intership')
		}
	}

	return
}