function resize (e) {
	let elem = e.target
	let div = document.createElement('div')
	
	div.style.width = elem.offsetWidth + 'px'
	div.style.height = elem.offsetHeight + 'px'
	div.style.overflow = 'auto'
	div.style.position = 'absolute'
	div.style.top = '0px'
	div.style.left = '0px'
	div.style.fontSize = getComputedStyle(elem).fontSize
	div.style.fontWeight = getComputedStyle(elem).fontWeight
	// div.style.lineHeight = getComputedStyle(elem).lineHeight
	div.style.border = '1px solid red'
	div.innerHTML = '<span>' + elem.value.replace(/\n/g, '<br />') + '</span>'
	
	document.body.appendChild(div)
	elem.rows = parseInt(div.firstChild.getClientRects().length + 0.5, 10)

	// console.log(div, elem.rows)
	document.body.removeChild(div)
}

const bind = (elem) => {
	if (process.browser) {
		elem.addEventListener('input', resize)
	}
}

const unbind = (elem) => {
	if (process.browser) {
		elem.removeEventListener('input', resize)
	}
}

export default {
	bind,
	unbind
}