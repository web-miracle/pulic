import Vue from 'vue'
import Adaptive from 'vue-adaptive'

Vue.use(Adaptive, {
	'desktop': {
		base: {
			width: 1440
		},
		from: {
			width: 1100
		},
		k: 1
	},
	'mobile': {
		base: {
			width: 350
		},
		from: {
			width: 320
		},
		to: {
			width: 500
		},
		k: 1
	}
})
