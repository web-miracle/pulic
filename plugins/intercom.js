import { getCookie } from '~/assets/cookie.js'
const base = require('~/plugins/axios.conf.json').baseURL
import axios from 'axios'

(function() {
	var w = window;
	var ic = w.Intercom;
	if (typeof ic === "function") {
		ic('reattach_activator');
		ic('update', intercomSettings);
	} else {
		var d = document;
		var i = function() {
			i.c(arguments)
		};
		i.q = [];
		i.c = function(args) {
			i.q.push(args)
		};
		w.Intercom = i;

		function l() {
			var s = d.createElement('script');
			s.type = 'text/javascript';
			s.async = true;
			s.src = 'https://widget.intercom.io/widget/nvntd9re';
			var x = d.getElementsByTagName('script')[0];
			x.parentNode.insertBefore(s, x);
		}
		if (w.attachEvent) {
			w.attachEvent('onload', l);
		} else {
			w.addEventListener('load', l, false);
		}
	}
})()

const secret = getCookie('secret')
window.Intercom("update");

if (secret) {
	axios.get(
		`${base}/auth/user`,
		{
			headers: {
				'authorization': `Bearer ${secret}`
			}
		}
	).then(res => {
		const user = res.data.data

		let name = (user.name || user.surname) ? user.name + ' ' + user.surname : user.nickname

		window.Intercom("boot", {
			app_id: "nvntd9re",
			name: name,
			email: user.email,
			created_at: user.created_at.date
		})
	}).catch(() => {
		window.Intercom("boot", {
			app_id: "nvntd9re"
		})
	})
}
