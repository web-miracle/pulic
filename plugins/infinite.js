import InfiniteLoading from 'vue-infinite-loading'
import Vue from 'vue'

const Infinite = {}
Infinite.install = (Vue_, options) => {
	Vue_.component('infinite-scroll', InfiniteLoading)
}

Vue.use(Infinite)