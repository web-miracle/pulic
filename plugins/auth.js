import Vue from 'vue'
// import VueAuth from '@websanova/vue-auth'

import axios from 'axios'
import VueAxios from 'vue-axios'

export default (router) => {
  Vue.use(VueAxios, axios)
  Vue.axios.defaults.baseURL = require('./axios.conf.json').baseURL
  Vue.axios.defaults.validateStatus = status => status > 0 
  
  // Vue.router = router.app.router

  // Vue.use(VueAuth, {
  //   auth: {
  //     request: function (req, token) {
  //       this.options.http._setHeaders.call(this, req, {Authorization: 'Bearer ' + token})
  //     },
  //     response: function (res) {
  //       this.user(res.data.data)
  //       return res.data.access_token
  //     }
  //   },
  //   tokenDefaultName: 'secret',
  //   tokenStore: ['cookie'],
  //   http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  //   router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  //   loginData: { url: '/auth/login', method: 'POST', fetchUser: false },
  //   logoutData: {url: '/auth/logout', method: 'GET'},
  //   fetchData: {url: '/auth/user', method: 'GET', enabled: false},
  //   refreshData: { enabled: false }
  // })  
}