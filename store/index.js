import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user.js'
import post from './modules/post.js'
import userPage from './modules/page/userPage.js'
import comment from './modules/comment.js'

Vue.use(Vuex)

const store = () => new Vuex.Store({
	modules: {
		user,
		post,
		userPage,
		comment
	}
})

export default store