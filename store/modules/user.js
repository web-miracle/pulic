import axios from 'axios'
import Vue from 'vue'
import { setCookie, deleteCookie, getCookie } from '~/assets/cookie.js'

const _ = {}
_.reduce = require('lodash/reduce')
_.forEach = require('lodash/forEach')

const state = {
	/**
	 * Информация о авторизованом пользователе
	 * @type {Object}
	 */
	userInfo: null,
	/**
	 * Ошибки при работе с модулем
	 * @type {[type]}
	 */
	errors: null,
	/**
	 * Сообщение об успешной регистрации пользователя
	 * @type {Boolean}
	 */
	registerSuccess: false,
	/**
	 * ID пользователя зарегистрированого через промо страницу
	 * @type {Integer}
	 */
	promoID: null,
	/**
	 * Остальные неавторизованые пользователи, информация о которых нам необходима
	 */
	users: {},
	userCount: 0,
	cookie: 1
}

const getters = {
	fullname (state) {
		if (state.userInfo) {
			return  (state.userInfo.name || state.userInfo.surname) ? state.userInfo.name + ' ' + state.userInfo.surname : state.userInfo.email
		} else {
			return ''
		}
	},

	/**
	 * Информация о том, что пользователь залогинен
	 * @type {Boolean}
	 */
	isAuth (state) {
		return state.userInfo !== null
	},

	avatar (state) {
		const user = state.userInfo
		
		if (user && user.avatar)
			return user.avatar
		
		if (user && user.gender && user.gender === 'f')
			return require('~/assets/img/default-user.female.png')

		return require('~/assets/img/default-user.male.png')
	},

	/**
	 * Bearer токен авторизованого пользователя
	 * @type {String}
	 */
	token (state) {
		if (process.browser && state.cookie)
			return !!getCookie('secret') ? getCookie('secret') : null

		return null
	}
}

const actions = {
	login ({commit}, data) {
		axios.post('/auth/login', data)
			.then(response => {
				if (response.status < 300) {
					commit('SET_USERINFO', response.data.data)
					commit('SET_COOKIE_TOKEN', response.data.access_token)

					if (process.browser)
						location.reload()
				} else {
					commit('TOKEN_FAIL')
					commit('SET_ERROR', {
						...response.data.errors,
						message: 'messages' in response.data ? response.data.messages[0] : false
					})
					return false
				}
			})
			.catch((e) => {
				commit('TOKEN_FAIL')
			})
	},
	logout ({commit}, data) {
		axios.get('/auth/logout')
			.then(response => {
				commit('SET_USERINFO', null)
				commit('TOKEN_FAIL', null)
			})
			.catch((e) => {
				commit('TOKEN_FAIL')
			})
	},
	async checkUser ({state, commit}, token) {
		try {
			let response = await axios.get('/auth/user', {
				headers: {
					'authorization': token ? `Bearer ${token}` : ''
				}
			})

			if (response.status < 300) {
				commit('SET_USERINFO', response.data.data)
			} else {
				throw Error('Такого пользователя нет')
			}
		} catch (e) {
			commit('TOKEN_FAIL')
		}
	},
	async getUser ({state, commit}, user) {
		try {
			let response = await axios.get(`/user/${user}?auth=${state.userInfo.id}`)

			if (response.status < 300) {
				commit('ADD_USER', response.data.data)
				return response.data.data
			} else {
				throw Error ('Ошибка с юзером')
			}
		} catch (e) {
			commit('SET_ERROR', {
				e
			})
		}
	},
	async singUp ({commit}, user) {
		try {
			let response = await axios.post('/auth/signup', user)

			if (response.status > 300) {
				commit('SET_ERROR', {
					...response.data.errors,
					message: 'messages' in response.data ? response.data.messages[0] : false
				})
				return false
			}

			commit('REGISTER_SUCCESS', true)

			setTimeout(() => {
				commit('REGISTER_SUCCESS', false)
			}, 5000)
		} catch (e) {
			commit('REGISTER_SUCCESS', false)
		}
	},
	async getUsersCount({commit}) {
		try {
			const response = await axios.get('/user/count')
			if (response.status < 300) {
				commit('SET_USER_COUNT', response.data.data.count)
			}
		} catch (e) {
			
		}
	},
	async signUpPromo ({commit}, data) {
		try {
			let response = await axios.post('/auth/signup/promo', data)
			commit('SET_PROMOID', response.data.data.user_id)
			return response.data
		} catch (e) {
			let error = e.response.data.errors ? e.response.data.errors : false
			return error
		}
	},
	update ({state, commit}, user) {
		console.log(state)
		const data = _.reduce(user, (result, value, key) => {
			if (value !== state.userInfo[key]) {
					result[key] = value

				if (key === 'active') {
					result.active = value ? 1 : 0
				}
			}
			return result
		}, {})

		delete data.password_confirmation
		delete data.created_at
		delete data.updated_at
		if (!data.password)
			delete data.password

		if (!user.avatar)
			delete data.avatar

		let form_data = new FormData()

		_.forEach(data, (value, key) => {
			if (key === 'flags') {
				form_data.append(key, JSON.stringify(value))
				return
			}

			form_data.append(key, value)
		})

		axios
			.post(`/user/${state.userInfo.id}`, form_data, {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			})
			.then(res => {
				if (res.status < 300) {
					commit('SET_USERINFO', res.data.data)
				}
			})
			.catch(() => {
				commit('TOKEN_FAIL')
			})
	},
	async makeSubscribe ({commit}, userId) {
		try {
			commit('CHANGE_SUBSCRIPTION', userId)
			let response = await axios.post(`/user/${userId}/subscribe`)

			if (response.status < 300) {
			} else {
				commit('CHANGE_SUBSCRIPTION', userId)
				throw Error(response.data)
			}
		} catch (e) {
			return e
		}
	},
	async promoTesting ({commit}, data) {
			try {
				let response = await axios.post('/auth/test', data)

				if (response.status < 500) {
					commit('SET_PROMOID')
					return response.data
				} else {
					throw Error('status answer > 500')
				}
			}
			catch (er) {
				let error = er.response.data.error ? er.response.data.error : false
				return error
			} 
		}
}

const mutations = {
	SET_USERINFO(state, data) {
		state.userInfo = data
	},
	SET_USER_COUNT(state, data) {
		state.userCount = data
	},
	SET_COOKIE_TOKEN(state, token) {
		if (token && token !== 'null') {

			if (process.browser) {
				axios.defaults.headers.common['authorization'] = token ? `Bearer ${token}` : ''
				setCookie('secret', token)
				state.cookie++
			}
		} else {
			state.userInfo = null
			delete axios.defaults.headers.common['authorization']

			if (process.browser) {
				deleteCookie('secret')
				state.cookie++
			}
		}
	},
	TOKEN_FAIL(state) {
		state.userInfo = null
		delete axios.defaults.headers.common['authorization']

		if (process.browser) {
			deleteCookie('secret')
			state.cookie++
		}
	},
	SET_ERROR(state, errors = null) {
		state.errors = errors
	},
	REGISTER_SUCCESS (state, bool = false) {
		state.registerSuccess = bool
	},
	SET_PROMOID (state, id = null) {
		state.promoID = id

		if (process.browser && id !== null) {
			setCookie('promoID', id)
		} else if (process.browser) {
			deleteCookie('promoID')
		}
	},
	ADD_USER(state, data) {
		Vue.set(state.users, data.id, data)
	},
	CHANGE_SUBSCRIPTION(state, userid) {
		if (state.users[userid]) {
			state.users[userid].subscribe = !state.users[userid].subscribe
			if (state.users[userid].subscribe) state.users[userid].subscribers++
			else state.users[userid].subscribers--
		}
	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}