import axios from 'axios'
import Vue from 'vue'

const state = {
	comments: {}
}

const getters = {}

//localhost:8000/api/comment?page=1&filters[model_id]=1
const actions = {
	async getComments ({commit}, {params, postID}) {
		try {
			const res = await axios.get(`comment?${params}`)
			if (res.status < 300) {
				commit('SET_COMMENTS', {
					comments: res.data.data,
					postID: postID
				})
				return res
			} else {
				throw Error('комментов нет, или что-то пошло не так =)')
			}
		} catch (e) {
			console.log(e)
		}
		
	},
	async createdComment ({commit}, data) {
		try {
			const response = await axios.post(`/comment`, data)
			if (response.status < 300) {
				commit('ADD_COMMENT', response.data.data)

				commit('post/UPDATE_COMMENTS_COUNT', {
					postID: data.model_id,
					difference: 1
				}, {root: true})
				return response.data
			} else {
				throw Error('action createdComment, что-то пошло не так')
			}
		} catch(e) {
			console.log(e)
		}
	},
	async deleteComment ({commit}, {id, model_id}) {
		try {
			const res = await axios.delete(`/comment/${id}`)

			if (res.status < 300) {
				commit('DELETE_COMMENT', {id: id, model_id: model_id})
			}
		} catch (e) {
			console.log(e)
		}
	}
}

const mutations = {
	SET_COMMENTS (state, {comments, postID}) {
		Vue.set(state.comments, postID, _.reverse(comments))
	},
	ADD_COMMENT (state, comment) {
		state.comments[comment.model_id].push(comment)
	},
	DELETE_COMMENT (state, {id, model_id}) {
		state.comments[model_id] = state.comments[model_id].filter(comment => comment.id !== id)
	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}
