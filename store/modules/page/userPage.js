const state = {
	/**
	 * Информация о текущем
	 * @type {Object}
	 */
	currentUser: null,
	/**
	 * Информация о авторизованом пользователе
	 * @type {Array}
	 */
	posts: []
}
const getters = {

}
const actions = {

}

const mutations = {
	SET_USERINFO(state, data) {
		state.currentUser = data
	},
	SET_USERPOSTS (state, data) {
		state.posts = data
	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}