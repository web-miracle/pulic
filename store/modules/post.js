import axios from 'axios'
import Vue from 'vue'

// const _ = {}
// _.find = require('lodash/find')

const state = {
	posts: [],
	archiveIdea: [],
	totalPosts: 0,
	seachPosts: [],
	searchWord: '',
	searchStatus: false,
	favePosts: [],
	popularPosts: []
}

const getters = {
	authorPosts (state) {
		// возможна избыточность вычислений. При сильных нагрузках и больших количествах авторов есть смысл переписать на возврат функции с переданным параметров author.id
		const posts = _.reduce(state.posts, (result, value) => {
			const user = value.user_id
			if (!(user in result)) {
				result[user] = []
			}
			result[user].push(value)
			return result
		}, {})
		const sort = _.forEach(posts, (value) => {
			value.sort((a, b) => {
				return Date.parse(b.created_at.date) - Date.parse(a.created_at.date)
			})
		})
		return sort
	},
	posts (state) {
		return _.reduce(state.posts, (result, value) => {
			// if (value.dislike && value.dislikes === 0) value.dislike = false
			if (!!value.image) {
				result.left.push(value)
			} else {
				result.right.push(value)
			}

			return result
		}, {left: [], right: []})
	}
}

const actions = {
	async getPosts ({commit, rootState}, params) {
		try {
			const auth_user = rootState.user.userInfo ? `conditions[auth]=${rootState.user.userInfo.id}` : ''
			const res = await axios.get(`post?${params}&${auth_user}&filters[published]=1`)
			if (res.status < 300) {
				commit('SET_POSTS', res.data.data)
				commit('SET_TOTAL', res.data.meta.total)
			} else {
				throw Error (res.data)
			}
		} catch(e) {
			console.log('Получаем посты', e)
		}
	},
	async addPosts ({commit, rootState}, params) {
		try {
			const auth_user = rootState.user.userInfo ? `conditions[auth]=${rootState.user.userInfo.id}` : ''
			const res = await axios.get(`post?${params}&${auth_user}&filters[published]=1`)

			if (res.status < 300) {
				commit('ADD_POSTS', res.data.data)
				commit('SET_TOTAL', res.data.meta.total)
			} else {
				throw Error (res.data)
			}
			return res.data.data
		} catch (e) {
			console.log('Добавляем посты', e)
		}
	},
	async deletePost ({commit}, post_id) {
		try {
			const res = await axios.delete(`post/` + post_id)
			if (res.status < 300) {
				commit('DELETE_POST', post_id)
			}
		} catch (e) {
			return e
		}
	},
	async likePost ({commit}, payload) {
		try {
			const response = await axios.post(`/post/${payload.id}/${payload.status}`)
			if (response.status < 300) {
				commit('LIKE_EDIT', { id: payload.id, data: response.data.data, status: payload.status})
				return response.data
			} else {
				// если в ответе ошибки, то убираем лайк
				throw Error('action likePost, что-то пошло не так')
			}
		} catch(e) {
			console.log(e)
			return false
		}
	},
	async dislikePost ({commit}, id = 0) {
		try {
			commit('DISLIKE_EDIT', id, 'dislike')
			const response = await axios.post(`/post/${id}/dislike`)

			if (response.status < 300) {
				return response.data
			} else {
				// если в ответе ошибки, то убираем дизлайк
				commit('LIKE_EDIT', id)
				throw Error('action dislikePost, что-то пошло не так')
			}
		} catch(e) {
			console.log(e)
			return false
		}
	},
	async createPost ({commit, rootState, state}, data) {
		try {
			const res = await axios.post(`/post/`, data)

			if (res.status < 300) {
				// const auth_user = rootState.user.userInfo ? `conditions[auth]=${rootState.user.userInfo.id}` : ''
				const temp = await axios.get(`post?filters[id]=${res.data.data.id}`) //убрать после замены ответа от бека при создании поста
				commit('ADD_POSTS', temp.data.data)
				commit('SET_TOTAL', state.totalPosts + 1)
				return true
			}
		} catch (e) {
			return e
		}
	},
	async editPost ({commit}, data) {
		try {
			const post = new FormData()
			_.forEach(data.post, (value, key) => {
				post.append(key, value)
			})

			const res = await axios.post(`/post/${data.post_id}`, post, {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			})

			if (res.status < 300) {
				commit('UPDATE_POST', res.data.data)
			} else {
				throw Error('Пост не получилось обновить')
			}
		} catch (e) {
			console.log(e)
		}
	}
}

const mutations = {
	SET_POSTS (state, posts) {
		state.posts = posts
	},
	ADD_POSTS (state, posts) {
		if (posts) {
			state.posts = _.uniqBy([...state.posts, ...posts], 'id')
		}
	},
	ADD_IDEA_POSTS (state, posts) {
		if (posts) {
			state.archiveIdea = _.uniqBy([...state.archiveIdea, ...posts], 'id')
		}
	},
	UPDATE_POST (state, post) {
		let current = _.findIndex(state.posts, ['id', post.id])
		Vue.set(state.posts, current, _.assign({}, state.posts[current], post))
	},
	DELETE_POST (state, postId) {
		state.posts = _.filter(state.posts, item => item.id !== postId)
	},
	LIKE_EDIT (state, payload) {
		const post = _.find(state.posts, {id: payload.id})
		if (payload.status === 'like') {
			post.like = !post.like
			if (post.dislike) post.dislike = false
		}
		if (payload.status === 'dislike') {
			post.dislike = !post.dislike
			if (post.like) post.like = false
		}
		post.likes = payload.data[0].likes
		post.dislikes = payload.data[0].dislikes
		post.rating = payload.data[0].rating
	},
	DISLIKE_EDIT (state, post_id) {
		const post = _.find(state.posts, {id: post_id})
		post.dislike = !post.dislike

		if (post.dislike)
			return post.dislikes++

		post.dislikes--
	},
	SET_TOTAL (state, total) {
		state.totalPosts = total
	},
	UPDATE_COMMENTS_COUNT(state, {postID, difference}) {
		let post = _.find(state.posts, ['id', postID])
		post.comments += difference
	},
	ADD_SEARCH (state, posts) {
		state.seachPosts = []
		state.seachPosts = posts
	},
	UPDATE_SEARCH_WORD (state, word) {
		state.searchWord = word
	},
	SET_SEARCH_STATUS (state, status) {
		state.searchStatus = status
	},
	SET_FAVE_POSTS (state, posts) {
		state.favePosts = posts
		state.posts = _.uniqBy([...state.posts, ...state.favePosts, ...posts], 'id')
	},
	ADD_FAVE_POSTS (state, posts) {
		if (posts) {
			state.favePosts = _.uniqBy([...state.favePosts, ...posts], 'id')
			state.posts = _.uniqBy([...state.posts, ...state.favePosts, ...posts], 'id')
		}
	},
	SET_POPULAR_POSTS (state, posts) {
		state.popularPosts = posts
		state.posts = _.uniqBy([...state.posts, ...state.popularPosts, ...posts], 'id')
	},
	ADD_POPULAR_POSTS (state, posts) {
		if (posts) {
			state.popularPosts = _.uniqBy([...state.popularPosts, ...posts], 'id')
			state.posts = _.uniqBy([...state.posts, ...state.popularPosts, ...posts], 'id')
		}
	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}