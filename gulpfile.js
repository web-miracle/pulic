const gulp = require('gulp')
// const spritesmith = require('gulp.spritesmith')
const svgSymbols = require('gulp-svg-symbols')
const svgmin = require('gulp-svgmin')
const rename = require('gulp-rename')
// const replace = require('gulp-replace')
// const clean = require('gulp-clean')

// ===========================================
//
//		SVG спрайт
//
// ===========================================
gulp.task('svg-sprite', function () {
	return gulp.src('./assets/sprites/svg/*.svg')
		.pipe(svgmin({
			plugins: [
				{removeDoctype: true},
				{removeComments: true},
				{cleanupAttrs: true}
			]
		}))
		.pipe(svgSymbols({
			svgClassname: 'svg-sprite',
			templates: ['default-svg']
		}))
		.pipe(rename('svg-sprite.svg'))
		.pipe(gulp.dest('./assets/img'))
})

gulp.task('copy-dist', function () {
	return gulp.src('./dist/**/*')
		.pipe(gulp.dest('../laravel/public/promo'))
})

gulp.task('watch', function () {
	gulp.watch('assets/sprites/svg/**/*.{svg}', gulp.series('svg-sprite'))
})


// ===========================================
//
//		Сборка проекта
//
// ===========================================
gulp.task('default', gulp.series('svg-sprite', 'watch'))
