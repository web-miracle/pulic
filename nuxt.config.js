const nodeExternals = require('webpack-node-externals')
const profile = require('./profile.js')
const webpack = require('webpack')

module.exports = {
  cache: false,
  /*
  ** Headers of the page
  */
  head: {
    title: 'СТАЯ 1.0',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'main project for dashboard project' }
    ],
    link: [
  	  { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
  	  { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=cyrillic' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  plugins: [
    {src: '~/plugins/adaptive.js', ssr: false},
    {src: '~/plugins/notification.js', ssr: false},
    {src: '~/plugins/intercom.js', ssr: false},
    {src: '~/plugins/yandex-metrica.js', ssr: false},
    // {src: '~/plugins/infinite.js', ssr: false},
    {src: '~/plugins/auth.js', ssr: true}
  ],
  router: {
    middleware: ['check-auth', 'userCount', 'intercom']
  },
  loading: { color: '#ffc001' },
  css: [
    {src: '~/assets/style/style.styl', lang: 'styl'}
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    plugins: [
      new webpack.ProvidePlugin({
        '_': 'lodash'
      })
    ],
    extend (config, { isDev, isClient, isServer }) {
      if (isDev && isClient) {
		config.resolve.extensions = ['.js', '.vue', '.json', '.css', '.styl', '.png', '.jpg']
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^Language/]
          })
        ]
      }
    },
  }
}

